package net.welen.mbeansnoop;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.ObjectName;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.MBeanServerNotification;
import javax.management.relation.MBeanServerNotificationFilter;
import javax.management.MBeanServerDelegate;

import java.io.IOException;

public class MBeanSnoop extends HttpServlet implements NotificationListener {
	private static final long serialVersionUID = 1L;	

	private MBeanServer server;	

	@Override
	public void init() throws ServletException {
		server = ManagementFactory.getPlatformMBeanServer();

		try {
			MBeanServerNotificationFilter filter = new MBeanServerNotificationFilter();
			filter.enableAllObjectNames();
			server.addNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this, filter, null);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	@Override
	public void destroy() {
		try {
			server.removeNotificationListener(MBeanServerDelegate.DELEGATE_NAME, this);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void handleNotification(Notification notification, Object handback) {
		if (notification instanceof MBeanServerNotification) {
			if (notification.getType().equals(MBeanServerNotification.REGISTRATION_NOTIFICATION)) {
				System.out.println("==================== Bean registred: " + notification);
			} else if (notification.getType().equals(MBeanServerNotification.UNREGISTRATION_NOTIFICATION)) {
				System.out.println("==================== MBean unregistred: " + notification);
			}
		}
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do nothing
	}

}
